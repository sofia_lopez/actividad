package Controller;

import DAO.clienteDAO;
import Model.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class managedCliente implements Serializable {

    private List<Cliente> listacliente;
    private clienteDAO clieDAO;
    private Cliente cliente;
    String mensaje = "";

    public List<Cliente> getListacliente() throws Exception {
        try {
            this.clieDAO = new clienteDAO();
            this.listacliente = this.clieDAO.llenarcliente();
        } catch (Exception e) {
            throw e;
        }
        return listacliente;
    }

    public void setListacliente(List<Cliente> listacliente) {
        this.listacliente = listacliente;
    }

    public clienteDAO getClieDAO() {
        return clieDAO;
    }

    public void setClieDAO(clienteDAO clieDAO) {
        this.clieDAO = clieDAO;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @PostConstruct
    public void init() {

        this.cliente = new Cliente();
    }

    public void guardar() {

        try {
            this.clieDAO = new clienteDAO();
            this.clieDAO.Insertar(cliente);
            this.cliente = new Cliente();
            this.mensaje = "Cliente registrado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarcliente(Cliente c) {
        this.cliente = c;
    }

    public void limpiar() {
        this.cliente = new Cliente();
    }

    public void editar() {

        try {
            this.clieDAO = new clienteDAO();
            this.clieDAO.updateEquipo(cliente);
            this.cliente = new Cliente();
            this.mensaje = "Cliente actualizado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminar(Cliente cc) {

        try {
            this.clieDAO = new clienteDAO();
            this.clieDAO.eliminar(cc);
            this.cliente = new Cliente();
            this.mensaje = "Cliente Eliminado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

}
