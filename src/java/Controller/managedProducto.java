package Controller;

import DAO.productoDAO;
import Model.*;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class managedProducto implements Serializable {

    private List<Producto> listaproducto;
    private productoDAO proDAO;
    private Producto producto;
    private Categoria categoria;
    String mensaje = "";

    public List<Producto> getListaproducto() throws Exception {
        try {
            this.proDAO = new productoDAO();
            this.listaproducto = this.proDAO.llenarproducto();
        } catch (Exception e) {
            throw e;
        }
        return listaproducto;
    }

    public void setListaproducto(List<Producto> listaproducto) {
        this.listaproducto = listaproducto;
    }

    public productoDAO getProDAO() {
        return proDAO;
    }

    public void setProDAO(productoDAO proDAO) {
        this.proDAO = proDAO;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @PostConstruct
    public void init() {
        this.producto = new Producto();
        this.categoria = new Categoria();
    }

    public void guardar() {

        try {
            this.proDAO = new productoDAO();
            this.proDAO.Insertar(producto);
            this.producto = new Producto();
            this.categoria = new Categoria();
            this.mensaje = "Producto registrado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarproducto(Producto p) {
        this.producto = p;
    }

    public void limpiar() {
        this.producto = new Producto();
        this.categoria = new Categoria();
    }

    public void editar() {

        try {
            this.proDAO = new productoDAO();
            this.proDAO.updateEquipo(producto);
            this.producto = new Producto();
            this.categoria = new Categoria();
            this.mensaje = "Producto actualizado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminar(Producto pp) {

        try {
            this.proDAO = new productoDAO();
            this.proDAO.eliminar(pp);
            this.producto = new Producto();
            this.categoria = new Categoria();
            this.mensaje = "Producto Eliminado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

}
