package Controller;

import DAO.categoriaDAO;
import Model.Categoria;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class managedCategoria implements Serializable {

    private List<Categoria> listacategoria;
    private categoriaDAO catDAO;
    private Categoria categoria;
    String mensaje = "";

    public List<Categoria> getListacategoria() throws Exception {
        try {
            this.catDAO = new categoriaDAO();
            this.listacategoria = this.catDAO.llenarcategoria();
        } catch (Exception e) {
            throw e;
        }
        return listacategoria;
    }

    public void setListacategoria(List<Categoria> listacategoria) {
        this.listacategoria = listacategoria;
    }

    public categoriaDAO getCatDAO() {
        return catDAO;
    }

    public void setCatDAO(categoriaDAO catDAO) {
        this.catDAO = catDAO;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @PostConstruct
    public void init() {
        this.categoria = new Categoria();
    }

    public void guardar() {

        try {
            this.catDAO = new categoriaDAO();
            this.catDAO.Insertar(categoria);
            this.categoria = new Categoria();
            this.mensaje = "Registrado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargarcategoria(Categoria g) {
        this.categoria = g;
    }

    public void limpiar() {
        this.categoria = new Categoria();
    }

    public void editar() {

        try {
            this.catDAO = new categoriaDAO();
            this.catDAO.updateEquipo(categoria);
            this.categoria = new Categoria();
            this.mensaje = "Actualizado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminar(Categoria tg) {

        try {
            this.catDAO = new categoriaDAO();
            this.catDAO.eliminar(tg);
            this.categoria = new Categoria();
            this.mensaje = "Cliente Eliminado con exito";
        } catch (Exception e) {
            this.mensaje = "Error " + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
}
