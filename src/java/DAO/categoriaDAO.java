package DAO;

import Model.Categoria;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class categoriaDAO extends Conexion {

    public void eliminar(Categoria cat) throws Exception {
        try {
            String query = "delete from  nombre where id_categoria=?";
            this.Conectar();
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            smt.setInt(1, cat.getId_categoria());
            smt.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }

    }

    public void updateEquipo(Categoria cat) throws Exception {
        try {
            String query = "update  cliente set nombre=?, descripcion=? where id_categoria=?";
            this.Conectar();
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            smt.setString(1, cat.getNombre());
            smt.setString(2, cat.getDescripcion());
            smt.setInt(3, cat.getId_categoria());
            smt.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }

    }

    public void Insertar(Categoria cat) throws Exception {
        try {
            this.Conectar();
            String query = "insert into categoria (nombre,descripcion) values (?,?)";
            PreparedStatement stm = this.getCnx().prepareStatement(query);
            stm.setString(1, cat.getNombre());
            stm.setString(2, cat.getDescripcion());
            stm.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }
    }

    public List<Categoria> llenarcategoria() throws Exception {
        List<Categoria> lista = new ArrayList<Categoria>();
        try {
            this.Conectar();
            String query = "Select id_categoria, nombre, descripcion from categoria";
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            ResultSet rt = smt.executeQuery();
            while (rt.next()) {
                Categoria ca = new Categoria();
                ca.setId_categoria(rt.getInt("id_categoria"));
                ca.setNombre(rt.getString("nombre"));
                ca.setDescripcion(rt.getString("descripcion"));
                lista.add(ca);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Desconectar();
        }
        return lista;
    }
}
