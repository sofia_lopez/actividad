package DAO;

import Model.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class productoDAO extends Conexion {

    public void eliminar(Producto pro) throws Exception {
        try {
            String query = "delete from  nombre where id_producto=?";
            this.Conectar();
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            smt.setInt(1, pro.getId_producto());
            smt.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }

    }

    public void updateEquipo(Producto pro) throws Exception {
        try {
            String query = "update  producto set nombre=?, precio=?, stock=? where id_producto=?";
            this.Conectar();
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            smt.setString(1, pro.getNombre());
            smt.setString(2, pro.getPrecio());
            smt.setString(3, pro.getStock());
            smt.setInt(3, pro.getId_producto());
            smt.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }

    }

    public void Insertar(Producto pro) throws Exception {
        try {
            this.Conectar();
            String query = "insert into producto (nombre,precio,stock) values (?,?,?)";
            PreparedStatement stm = this.getCnx().prepareStatement(query);
            stm.setString(1, pro.getNombre());
            stm.setString(2, pro.getPrecio());
            stm.setString(3, pro.getStock());
            stm.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }
    }

    public List<Producto> llenarproducto() throws Exception {
        List<Producto> lista = new ArrayList<Producto>();
        try {
            this.Conectar();
            String query = "Select id_producto, nombre, precio, stock, id_categoria from producto";
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            ResultSet rt = smt.executeQuery();
            while (rt.next()) {
                Producto pro = new Producto();
                Categoria ca = new Categoria();
                pro.setId_producto(rt.getInt("id_producto"));
                pro.setNombre(rt.getString("nombre"));
                pro.setPrecio(rt.getString("precio"));
                pro.setStock(rt.getString("stock"));
                ca.setId_categoria(rt.getInt("id_categoria"));
                pro.getCategoria();
                lista.add(pro);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Desconectar();
        }
        return lista;
    }
}
