package DAO;

import Model.Cliente;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class clienteDAO extends Conexion {

    public void eliminar(Cliente cli) throws Exception {
        try {
            String query = "delete from  cliente where id_cliente=?";
            this.Conectar();
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            smt.setInt(1, cli.getId_cliente());
            smt.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }

    }

    public void updateEquipo(Cliente cli) throws Exception {
        try {
            String query = "update  cliente set nombre=?, apellido=?, direccion=?, fecha_nacimiento=?, telefono=?, email=? where id_cliente=?";
            this.Conectar();
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            smt.setString(1, cli.getNombre());
            smt.setString(2, cli.getApellido());
            smt.setString(3, cli.getDireccion());
            smt.setDate(4, (Date) cli.getFecha_nacimiento());
            smt.setString(5, cli.getTelefono());
            smt.setString(6, cli.getEmail());
            smt.setInt(7, cli.getId_cliente());
            smt.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }

    }

    public void Insertar(Cliente cli) throws Exception {
        try {
            this.Conectar();
            String query = "insert into cliente (nombre,apellido,direccion,fecha_nacimiento,telefono,email) values (?,?,?,?,?,?)";
            PreparedStatement stm = this.getCnx().prepareStatement(query);
            stm.setString(1, cli.getNombre());
            stm.setString(2, cli.getApellido());
            stm.setString(3, cli.getDireccion());
            stm.setDate(4, (Date) cli.getFecha_nacimiento());
            stm.setString(5, cli.getTelefono());
            stm.setString(6, cli.getEmail());
            stm.executeUpdate();
        } catch (Exception ex) {
            throw ex;
        } finally {
            this.Desconectar();
        }
    }

    public List<Cliente> llenarcliente() throws Exception {
        List<Cliente> lista = new ArrayList<Cliente>();
        try {
            this.Conectar();
            String query = "Select id_paciente, paciente from paciente";
            PreparedStatement smt = this.getCnx().prepareStatement(query);
            ResultSet rt = smt.executeQuery();
            while (rt.next()) {
                Cliente cli = new Cliente();
                cli.setId_cliente(rt.getInt("id_cliente"));
                cli.setNombre(rt.getString("nombre"));
                cli.setApellido(rt.getString("apellido"));
                cli.setDireccion(rt.getString("direccion"));
                cli.setFecha_nacimiento(rt.getDate("fecha_nacimiento"));
                cli.setTelefono(rt.getString("telefono"));
                cli.setEmail(rt.getString("email"));
                lista.add(cli);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Desconectar();
        }
        return lista;
    }

}
